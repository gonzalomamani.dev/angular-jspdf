import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListsComponent} from './components/lists/lists.component';
import {TemplateComponent} from './components/template/template.component';
import {PropiedadIndustrialComponent} from "./components/propiedad-industrial/propiedad-industrial.component";


const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', redirectTo: 'lists', pathMatch: 'full'},
      {path: 'lists', component: ListsComponent},
      {path: 'template', component: TemplateComponent},
      {path: 'propiedad-industrial', component: PropiedadIndustrialComponent},
      {path: '**', redirectTo: 'lists', pathMatch: 'full'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
