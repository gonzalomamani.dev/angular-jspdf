import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListsComponent } from './components/lists/lists.component';
import { TemplateComponent } from './components/template/template.component';
import { PropiedadIndustrialComponent } from './components/propiedad-industrial/propiedad-industrial.component';

@NgModule({
  declarations: [
    AppComponent,
    ListsComponent,
    TemplateComponent,
    PropiedadIndustrialComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
